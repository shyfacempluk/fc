  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Barang</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 50px">Kode</th>
                      <th>Nama</th>
                      <th style="width: 160px">Harga</th>
                      <th style="width: 100px">Stok</th>
                      <th style="width: 100px">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <a href="<?=base_url('home/tambah_barang')?>"><button type="button" class="btn btn-success mb-2"><i class="fas fa-plus"></i></button></a>
                    <?php
                      // print_r($barang);
                      $no = 1;
                      foreach ($barang as $key => $value) {
                    ?>
                    <tr>
                      <td><?=$no ?>.</td>
                      <td><?=$value['kode'] ?></td>
                      <td>
                        <?=$value['nama'] ?>
                      </td>
                      <td><span class="badge bg-success"><?=$value['harga_beli'] ?></span> / <span class="badge bg-danger"><?=$value['harga_jual'] ?></span></td>
                      <td><?=$value['jumlah_stok'] ?></td>
                      <td>
                          <a href="<?=base_url('home/edit_barang/').$value['id']?>"><button type="button" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i></button></a>
                          <a href="<?=base_url('home/hapus_barang/').$value['id']?>"><button type="button" class="btn btn-danger btn-sm"><i class="fas fa-times"></i></button></a>
                      </td>
                    </tr>
                    <?php 
                        $no++;
                      } 
                    ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
                </ul>
              </div> -->
            </div>
          </div>
          <!-- /.col-md-6 -->
          <!-- <div class="col-lg-6">
]
          </div> -->
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

