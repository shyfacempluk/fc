  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Barang</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Form Barang</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Barang</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php
              // print_r($barang);
              $brg = $barang[0];
              ?>
              <form method="POST" action="<?=base_url('home/simpan_edit_barang/').$brg['id']?>">
                <div class="card-body">
                  <div class="form-group">
                    <label for="kode">Kode Barang</label>
                    <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Barang" value="<?=$brg['kode']?>">
                  </div>
                  <div class="form-group">
                    <label for="namabarang">Nama Barang</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Barang" value="<?=$brg['nama']?>">
                  </div>
                  <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" name="jumlah" placeholder="Jumlah" value="<?=$brg['jumlah_stok']?>">
                  </div>
                  <div class="form-group">
                    <label for="hbeli">Harga Beli</label>
                    <input type="number" class="form-control" id="hbeli" name="hbeli" placeholder="Harga Beli" value="<?=$brg['harga_beli']?>">
                  </div>
                  <div class="form-group">
                    <label for="hjual">Harga Jual</label>
                    <input type="number" class="form-control" id="hjual" name="hjual" placeholder="Harga Jual" value="<?=$brg['harga_jual']?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
          <!-- /.col-md-6 -->
          <!-- <div class="col-lg-6">
]
          </div> -->
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

