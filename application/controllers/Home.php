<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if (!isset($_SESSION['logged_in']) || $_SESSION['logged_in'] != 1) {
			$this->session->sess_destroy();
			redirect('user');
		}
    }

	public function index() {
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/index');
		$this->load->view('template/footer');

	}

	public function barang() {
		$query['barang'] = $this->db->get('barang')->result_array();
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/barang', $query);
		$this->load->view('template/footer');
	}

	// public function form_barang() {
	// 	$this->load->view('template/header');
	// 	$this->load->view('template/sidebar');
	// 	$this->load->view('template/form_barang');
	// 	$this->load->view('template/footer');
	// }

	public function tambah_barang() {
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/form_tambah_barang');
		$this->load->view('template/footer');
	}

	public function simpan_barang() {
		// print_r($_POST);
		$data = array(
	        'kode' => $_POST['kode'],
	        'nama' => $_POST['nama'],
	        'harga_beli' => $_POST['hbeli'],
	        'harga_jual' => $_POST['hjual'],
	        'jumlah_stok' => $_POST['jumlah'],
		);

		$this->db->insert('barang', $data);
		redirect('home/barang');
	}

	public function edit_barang() {
		$id =  $this->uri->segment(3, 0);
		$query['barang'] = $this->db->get_where('barang', 
				array(
					'id' => $id
				), 1
			)->result_array();
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/form_edit_barang', $query);
		$this->load->view('template/footer');
	}

	public function simpan_edit_barang() {
		// print_r($_POST);
		$id =  $this->uri->segment(3, 0);
		$data = array(
	        'kode' => $_POST['kode'],
	        'nama' => $_POST['nama'],
	        'harga_beli' => $_POST['hbeli'],
	        'harga_jual' => $_POST['hjual'],
	        'jumlah_stok' => $_POST['jumlah'],
		);
		$this->db->set($data);
		$this->db->where('id', $id);
		$this->db->update('barang');
		redirect('home/barang');
	}

	public function hapus_barang() {
		$id =  $this->uri->segment(3, 0);
		$this->db->where('id', $id);
		$this->db->delete('barang');
		// $product_id = $this->uri->segment(3, 0);
		redirect('home/barang');
	}

	public function transaksi() {
		$query['barang'] = $this->db->get('barang')->result_array();
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/transaksi', $query);
		$this->load->view('template/footer');
	}

	public function simpan_transaksi() {
		print_r($_POST);
		$query['barang'] = $this->db->get_where('barang', 
				array(
					'id' => $_POST['barang']
				), 1
			)->result_array();

		$data = array(
	        'kode_barang' => $query['barang'][0]['kode'],
	        'jenis_transaksi' => $_POST['jenis'],
	        'tanggal' => date("Y-m-d H:i:s"),
	        'harga_barang' => $query['barang'][0]['harga_jual'],
	        'jumlah' => $_POST['jumlah'],
	        'user' => $_SESSION['id']
		);

		$this->db->insert('transaksi', $data);

		if ($_POST['jenis'] == 'J') {
			$data = array(
		        'jumlah_stok' => $query['barang'][0]['jumlah_stok'] - $_POST['jumlah'],
			);
			
		}
		if ($_POST['jenis'] == 'B') {
			$data = array(
		        'jumlah_stok' => $query['barang'][0]['jumlah_stok'] + $_POST['jumlah'],
			);
		}

		$this->db->set($data);
		$this->db->where('id', $query['barang'][0]['id']);
		$this->db->update('barang');
		// print_r($query);
		redirect('home/laporan');
	}

	public function laporan() {
		$query['transaksi'] = $this->db->query('
			SELECT transaksi.*,transaksi.harga_barang*transaksi.jumlah as total_harga ,barang.nama, barang.jumlah_stok AS sisa_stok FROM transaksi left join barang ON barang.kode = transaksi.kode_barang ORDER BY id DESC
		')->result_array();
		$this->load->view('template/header');
		$this->load->view('template/sidebar');
		$this->load->view('template/laporan', $query);
		$this->load->view('template/footer');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('user');
	}
}