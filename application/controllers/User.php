<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == 1) {
			redirect('home');
		}
    }

	public function index() {
		$this->load->view('pages/login');
	}

	public function register() {
		$this->load->view('pages/register');
	}

	public function simpan_registrasi() {
		// print_r($_POST);
		$data = array(
	        'username' => $_POST['username'],
	        'password' => MD5($_POST['password']),
	        'nama' => $_POST['nama'],
		);

		$query = $this->db->get_where('user', 
			array(
				'username' => $_POST['username']
			), 1
		)->result_array();

		if (count($query) > 0) {
			$this->session->set_flashdata('message', 'Pendaftaran gagal, username "'.$_POST['username'].'" sudah digunakan.');
			$this->load->view('pages/register');
		} else {
			if ($_POST['password'] == $_POST['ulang']) {
				$this->db->insert('user', $data);
				$this->session->set_flashdata('message', 'Pendaftaran Berhasil, silahkan login.');
				$this->load->view('pages/login');
			} else {
				$this->session->set_flashdata('message', 'Pendaftaran gagal, silahkan isi data dengan benar.');
				$this->load->view('pages/register');
			}
		}
	}

	public function cek_login() {
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('message', 'Username / Password harus diisi.');
		} else {
			$query = $this->db->get_where('user', 
				array(
					'username' => $_POST['username'],
					'password' => MD5($_POST['password']),
				), 1
			)->result_array();
			if (count($query) > 0) {
				$newdata = array(
					'id'  => $query[0]['id'],
			        'username'  => $query[0]['username'],
			        'nama'     => $query[0]['nama'],
			        'logged_in' => TRUE
				);
				$this->session->set_userdata($newdata);
				redirect('/home');
			}
			$this->session->set_flashdata('message', 'Username / Password salah.');
		}
		$this->load->view('pages/login');
	}
}